package gui;

import java.awt.Color; 
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import javax.swing.SwingUtilities;

import org.jxmapviewer.JXMapViewer;
import org.jxmapviewer.viewer.GeoPosition;

import javafx.application.Application;
import javafx.embed.swing.SwingNode;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import modelo.CiaAerea;
import modelo.Aeronave;
import modelo.Aeroporto;
import modelo.Geo;
import modelo.GerenciadorAeroportos;
import modelo.GerenciadorCias;
import modelo.GerenciadorPaises;
import modelo.GerenciadorRotas;
import modelo.Paises;
import modelo.Rota;
import modelo.VooDireto;
import modelo.VooEscalas;
import modelo.GerenciadorAeronaves;

public class JanelaFX extends Application {

	final SwingNode mapkit = new SwingNode();

	private GerenciadorCias gerCias;
	private GerenciadorAeroportos gerAero;
	private GerenciadorRotas gerRotas;
	private GerenciadorAeronaves gerJato;
	private GerenciadorMapa gerenciador;
	private GerenciadorPaises gerPais;
	private EventosMouse mouse;

	@Override
	public void start(Stage primaryStage) throws Exception {

		setup();

		GeoPosition poa = new GeoPosition(-30.05, -51.18);
		gerenciador = new GerenciadorMapa(poa, GerenciadorMapa.FonteImagens.VirtualEarth);
		mouse = new EventosMouse();
		gerenciador.getMapKit().getMainMap().addMouseListener(mouse);
		gerenciador.getMapKit().getMainMap().addMouseMotionListener(mouse);

		createSwingContent(mapkit);
		
		BorderPane pane = new BorderPane();
		GridPane leftPane = new GridPane();
		leftPane.setAlignment(Pos.CENTER);
		leftPane.setHgap(5);
		leftPane.setVgap(5);
		leftPane.setPadding(new Insets(10, 10, 10, 10));
		
		ChoiceBox<String> box = new ChoiceBox<>();
		ChoiceBox<String> box2 = new ChoiceBox<>();
		ChoiceBox<String> box5 = new ChoiceBox<>();
		ChoiceBox<String> box6 = new ChoiceBox<>();
		ChoiceBox<String> box3 = new ChoiceBox<>();
		ChoiceBox<String> box4 = new ChoiceBox<>();
		for(Paises p: gerPais.listarTodos()){
			String nome= p.getNome();
			box.getItems().add(nome);
		}
		
		leftPane.add(new Label("Selecione um pa�s"), 0, 0);
		leftPane.add(box, 0, 1);
		Button btnConsulta2 = new Button("Exibir estimativa de tr�fego do pa�s");
		leftPane.add(btnConsulta2, 0, 2);
		leftPane.add(new Label("Selecione uma cia para exibir a estimativa"), 0, 3);
		
		leftPane.add(box2, 0, 4);
		Button btnConsulta3 = new Button("Desenha Aeroportos e Rotas de uma Cia");
		leftPane.add(btnConsulta3, 0, 5);
		
		
		
		leftPane.add(new Label("Exibir estimativa de tr�fego geral"), 0, 6);
		Button btnConsulta1 = new Button("Exibir estimativa");
		leftPane.add(btnConsulta1, 0, 7);
		
		
		
		for(CiaAerea c: gerCias.listarTodas()){
			String nome = c.getNome();
			box2.getItems().add(nome);
		}
		
		
	
		
		for(int i=1; i <= 24; i++){
			box5.getItems().add(String.valueOf(i));
			
		}
		
		
		
		for(Aeroporto a: gerAero.listarTodos()){
			String codigo = a.getCodigo();
			box6.getItems().add(codigo);
			
		}
		for(Aeroporto a: gerAero.listarTodos()){
			String codigo = a.getCodigo();
			box4.getItems().add(codigo);
			
		}
		for(Aeroporto a: gerAero.listarTodos()){
			String codigo = a.getCodigo();
			box3.getItems().add(codigo);
			
		}
		Button btnConsulta4 = new Button("Exibir Rotas Possiveis");
		leftPane.add(btnConsulta4, 0, 13);
		leftPane.add(new Label("Selecione Aeroporto Origem"), 0, 9);
		leftPane.add(box3, 0, 10);
		leftPane.add(new Label("Selecione Aeroporto Destino"), 0, 11);
		leftPane.add(box4, 0, 12);

		Button btnConsulta5 = new Button("Enviar");
		leftPane.add(new Label("Aeroportos alcan�aveis apartir de um aeroporto de origem em um determinado tempo m�ximo:"), 0, 14);
		leftPane.add(new Label("Aeroportos origem: "), 0, 16);
		leftPane.add(box5, 0, 19);
		leftPane.add(new Label("Tempo m�ximo de voo: "), 0, 18);
		leftPane.add(box6, 0, 17);
		leftPane.add(btnConsulta5, 0, 20);
		
		
		
		btnConsulta1.setOnAction(e -> {
			consulta2();
		});
		btnConsulta3.setOnAction(e -> {
			consulta1(getchoice(box2));
		});
		 btnConsulta2.setOnAction(e ->{
		 
			 consulta21(getchoice(box));
		 });
		 btnConsulta4.setOnAction(e->{
			 consulta3(getchoice(box3), getchoice(box4));
		 });
	
		btnConsulta5.setOnAction(e->{
			consulta4(getchoice(box6), Integer.parseInt(getchoice(box5)));
		});

		pane.setCenter(mapkit);
		pane.setLeft(leftPane);
		

		Scene scene = new Scene(pane, 1200, 600);
		primaryStage.setScene(scene);
		primaryStage.setTitle("Mapas com JavaFX");
		primaryStage.show();

	}

	private String getchoice(ChoiceBox<String> box) {
		String escolha = box.getValue();
		return escolha;
	}

	// Inicializando os dados aqui...
	private void setup() {

		gerCias = new GerenciadorCias();
		gerAero = new GerenciadorAeroportos();
		gerRotas = new GerenciadorRotas();
		gerJato = new GerenciadorAeronaves();
		gerPais = new GerenciadorPaises();

		try {
			gerCias.carregaDados();
		} catch (IOException e) {
			System.out.println("Impossível ler airlines.dat!");
			System.out.println("Msg: " + e);
			System.exit(1);
		}

		Path path1 = Paths.get("countries.dat");
		try (Scanner sc = new Scanner(Files.newBufferedReader(path1, Charset.forName("utf8")))) {
			sc.useDelimiter("[;\n]"); // separadores: ; e nova linha
			String header = sc.nextLine(); // pula cabe�alho
			String codigo, nome;
			while (sc.hasNext()) {

				codigo = sc.next();
				nome = sc.next();
				Paises pais = new Paises(codigo, nome);
				gerPais.adicionar(pais);
			}
		} catch (IOException x) {
			System.err.format("Erro de E/S: %s%n", x);
		}

		Path path3 = Paths.get("airports.dat");
		try (Scanner sc = new Scanner(Files.newBufferedReader(path3, Charset.forName("utf8")))) {
			sc.useDelimiter("[;\n]");
			String header = sc.nextLine();
			String idpais, codigo, nome;
			double latitude;
			double longitude;
			while (sc.hasNext()) {

				codigo = sc.next();
				latitude = Double.parseDouble(sc.next());
				longitude = Double.parseDouble(sc.next());
				nome = sc.next();
				idpais = sc.next();
				Geo geo = new Geo(latitude, longitude);
				Aeroporto aer = new Aeroporto(codigo, nome, geo, idpais);
				gerAero.adicionar(aer);
			}
		} catch (IOException x) {
			System.err.format("Erro de E/S: %s%n", x);
		}

		Path path4 = Paths.get("equipment.dat");
		try (Scanner sc = new Scanner(Files.newBufferedReader(path4, Charset.forName("utf8")))) {
			sc.useDelimiter("[;\n]"); // separadores: ; e nova linha
			String header = sc.nextLine(); // pula cabe�alho
			String codigo, desc;
			int capacidade;
			while (sc.hasNext()) {
				codigo = sc.next();
				desc = sc.next();
				capacidade = Integer.parseInt(sc.next());

				Aeronave aer = new Aeronave(codigo, desc, capacidade);
				gerJato.adicionar(aer);

			}
		} catch (IOException x) {
			System.err.format("Erro de E/S: %s%n", x);
		}

		Path path8 = Paths.get("airlines.dat");
		try (Scanner sc = new Scanner(Files.newBufferedReader(path8, Charset.forName("utf8")))) {
			sc.useDelimiter("[;\n]"); // separadores: ; e nova linha
			String header = sc.nextLine(); // pula cabe�alho
			String nome, codigo;
			while (sc.hasNext()) {
				codigo = sc.next();
				nome = sc.next();

				CiaAerea cia = new CiaAerea(codigo, nome);
				gerCias.adicionar(cia);

			}
		} catch (IOException x) {
			System.err.format("Erro de E/S: %s%n", x);
		}

		Path path5 = Paths.get("routes.dat");
		try (Scanner sc = new Scanner(Files.newBufferedReader(path5, Charset.forName("utf8")))) {
			sc.useDelimiter("[;\n]"); // separadores: ; e nova linha
			String header = sc.nextLine(); // pula cabe�alho
			String ciaaerea, from, to, code, stops, equipment;

			while (sc.hasNext()) {

				ciaaerea = sc.next();
				from = sc.next();
				to = sc.next();
				code = sc.next();
				stops = sc.next();
				equipment = sc.next();
				equipment = "" + equipment.charAt(0) + equipment.charAt(1) + equipment.charAt(2);
				
				CiaAerea cia = gerCias.buscarCodigo(ciaaerea);
				Aeroporto orig = gerAero.buscarCodigo(from);
				Aeroporto dest = gerAero.buscarCodigo(to);
				Aeronave nave = gerJato.buscarCodigo(equipment);

				Rota rotao = new Rota(cia, orig, dest, nave);

				gerRotas.adicionar(rotao);

			}
		} catch (IOException x) {
			System.err.format("Erro de E/S: %s%n", x);
		}
		for (Rota r : gerRotas.listarTodas()) {
			r.getOrigem().acumTrafego();
			r.getDestino().acumTrafego();
		}

		Aeroporto poa = new Aeroporto("POA", "Salgado Filho Intl Apt", new Geo(-29.9939, -51.1711), "BR");
		Aeroporto gru = new Aeroporto("GRU", "São Paulo Guarulhos Intl Apt", new Geo(-23.4356, -46.4731), "BR");
		Aeroporto mia = new Aeroporto("MIA", "Miami International Apt", new Geo(25.7933, -80.2906), "US");
		gerAero.adicionar(poa);
		gerAero.adicionar(gru);
		gerAero.adicionar(mia);

	}


	private class EventosMouse extends MouseAdapter {
		private int lastButton = -1;

		@Override
		public void mousePressed(MouseEvent e) {
			JXMapViewer mapa = gerenciador.getMapKit().getMainMap();
			GeoPosition loc = mapa.convertPointToGeoPosition(e.getPoint());
			lastButton = e.getButton();
			List<MyWaypoint> lstPoints = new ArrayList<>();
			if (lastButton == MouseEvent.BUTTON3) {
				gerenciador.clear();
				gerenciador.setPosicao(loc);
				int count =0;
				for (Aeroporto p : gerAero.listarTodos()) {
					if(p.getLocal().distancia(new Geo(loc.getLatitude(),loc.getLongitude())) < 5){
						lstPoints.add(new MyWaypoint(Color.BLUE, p.getNome(), p.getLocal(), 20));
						gerenciador.setPontos(lstPoints);
						count++;
					}
					if(count>0){
						break;
					}
				}
				gerenciador.getMapKit().repaint();
			}
		}
	}

	private void createSwingContent(final SwingNode swingNode) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				swingNode.setContent(gerenciador.getMapKit());
			}
		});
	}

	public static void main(String[] args) {
		launch(args);
	}

	public void consulta1(String s) {
		GeoPosition pos = gerenciador.getPosicao();
		List<MyWaypoint> lstPoints = new ArrayList<>();
		CiaAerea cia = gerCias.buscarNome(s);

		for (Rota r : gerRotas.listarTodas()) {

			if (r.getCia().equals(cia)){

				lstPoints.add(new MyWaypoint(Color.BLUE, r.getOrigem().getNome(), r.getOrigem().getLocal(), 10));
				lstPoints.add(new MyWaypoint(Color.BLUE, r.getDestino().getNome(), r.getDestino().getLocal(), 10));
				
				gerenciador.setPontos(lstPoints);

				Tracado tr = new Tracado();
				// Adicionando as mesmas localizações de antes
				tr.addPonto(r.getOrigem().getLocal());
				tr.addPonto(r.getDestino().getLocal());
				tr.setWidth(4); // largura do traçado
				tr.setCor(Color.RED); // cor do traçado

				// E adicionando o traçado...
				gerenciador.addTracado(tr);
				gerenciador.getMapKit().repaint();

			}
		}

	}

	public void consulta2() {
		
		GeoPosition pos = gerenciador.getPosicao();
		List<MyWaypoint> lstPoints = new ArrayList<>();

		for (Aeroporto r : gerAero.listarTodos()) {
			if (r.getTrafego() > 1000) {
				lstPoints.add(new MyWaypoint(Color.RED, r.getNome(), r.getLocal(), 10));
			} else if (r.getTrafego() > 500) {
				lstPoints.add(new MyWaypoint(Color.BLUE, r.getNome(), r.getLocal(), 5));

			} else if (r.getTrafego() > 250) {
				lstPoints.add(new MyWaypoint(Color.YELLOW, r.getNome(), r.getLocal(), 3));

			} else {
				lstPoints.add(new MyWaypoint(Color.GREEN, r.getNome(), r.getLocal(), 1));

			}
		}
		gerenciador.setPontos(lstPoints);
	}

	public void consulta21(String nomePais) {
		GeoPosition pos = gerenciador.getPosicao();
		List<MyWaypoint> lstPoints = new ArrayList<>();
		
		String idpais = "";
		
		for(Paises p: gerPais.listarTodos())
		{
			if(p.getNome().equals(nomePais))
			{
				idpais = p.getCodigo();
				break;
			}
		}

		for (Aeroporto r : gerAero.listarTodos()) {
			if (idpais.equals(r.getIdpais())) {
				if (r.getTrafego() > 1000) {
					lstPoints.add(new MyWaypoint(Color.RED, r.getNome(), r.getLocal(), 10));
				} else if (r.getTrafego() > 500) {
					lstPoints.add(new MyWaypoint(Color.BLUE, r.getNome(), r.getLocal(), 5));

				} else if (r.getTrafego() > 250) {
					lstPoints.add(new MyWaypoint(Color.YELLOW, r.getNome(), r.getLocal(), 3));

				} else {
					lstPoints.add(new MyWaypoint(Color.GREEN, r.getNome(), r.getLocal(), 2));

				}
			}
		}
		gerenciador.setPontos(lstPoints);
	}

	public void consulta3(String aero1, String aero2) {
		GeoPosition pos = gerenciador.getPosicao();
		List<MyWaypoint> lstPoints = new ArrayList<>();
		for (Rota r : gerRotas.listarTodas()) {
			if (r.getOrigem().equals(gerAero.buscarCodigo(aero1))
					&& r.getDestino().equals(gerAero.buscarCodigo(aero2))) {
				System.out.println("Entreiiiiiiiiiiiiiiiiiiiiiiiiii");
				lstPoints.add(new MyWaypoint(Color.RED, gerAero.buscarCodigo(aero1).getNome(),
						gerAero.buscarCodigo(aero1).getLocal(), 10));
				lstPoints.add(new MyWaypoint(Color.RED, gerAero.buscarCodigo(aero2).getNome(),
						gerAero.buscarCodigo(aero2).getLocal(), 10));
				gerenciador.setPontos(lstPoints);
				Tracado tr = new Tracado();
				// Adicionando as mesmas localizações de antes
				tr.addPonto(gerAero.buscarCodigo(aero1).getLocal());
				tr.addPonto(gerAero.buscarCodigo(aero2).getLocal());
				tr.setWidth(4); // largura do traçado
				tr.setCor(Color.RED); // cor do traçado

				// E adicionando o traçado...
				gerenciador.addTracado(tr);
				gerenciador.getMapKit().repaint();

			}
		}

		for (Aeroporto a : gerAero.listarTodos()) {
			for (Rota r1 : gerRotas.listarTodas()) {
				if (r1.getOrigem().equals(gerAero.buscarCodigo(aero1)) && r1.getDestino().equals(a)) {
					for (Rota r2 : gerRotas.listarTodas()) {
						if (r2.getOrigem().equals(a) && r2.getDestino().equals(gerAero.buscarCodigo(aero2))) {
							System.out.println("Entreiiiiiiiiiiiiiiiiiiiiiiiii222222222222222222i");
							lstPoints.add(new MyWaypoint(Color.RED, gerAero.buscarCodigo(aero1).getNome(),
									gerAero.buscarCodigo(aero1).getLocal(), 10));
							lstPoints.add(new MyWaypoint(Color.RED, a.getNome(), a.getLocal(), 10));
							gerenciador.setPontos(lstPoints);
							Tracado tr = new Tracado();
							// Adicionando as mesmas localizações de antes
							tr.addPonto(gerAero.buscarCodigo(aero1).getLocal());
							tr.addPonto(a.getLocal());
							tr.setWidth(4); // largura do traçado
							tr.setCor(Color.RED); // cor do traçado

							// E adicionando o traçado...
							gerenciador.addTracado(tr);
							gerenciador.getMapKit().repaint();

							lstPoints.add(new MyWaypoint(Color.RED, a.getNome(), a.getLocal(), 10));
							lstPoints.add(new MyWaypoint(Color.RED, gerAero.buscarCodigo(aero1).getNome(),
									gerAero.buscarCodigo(aero1).getLocal(), 10));
							gerenciador.setPontos(lstPoints);
							Tracado tr2 = new Tracado();
							tr2.addPonto(a.getLocal());
							tr2.addPonto(gerAero.buscarCodigo(aero1).getLocal());
							tr2.setWidth(4); // largura do traçado
							tr2.setCor(Color.RED); // cor do traçado

							// E adicionando o traçado...
							gerenciador.addTracado(tr2);
							gerenciador.getMapKit().repaint();

						}
					}
				}
			}
		}

		for (Aeroporto a : gerAero.listarTodos()) {
			if (!a.equals(gerAero.buscarCodigo(aero2))) {
				for (Aeroporto b : gerAero.listarTodos()) {
					if (!b.equals(gerAero.buscarCodigo(aero1))) {
						for (Rota r1 : gerRotas.listarTodas()) {
							if (r1.getOrigem().equals(gerAero.buscarCodigo(aero1)) && r1.getDestino().equals(a)) {
								for (Rota r2 : gerRotas.listarTodas()) {
									if (r2.getOrigem().equals(a) && r2.getDestino().equals(b)) {
										for (Rota r3 : gerRotas.listarTodas()) {
											if (r3.getOrigem().equals(b)
													&& r3.getDestino().equals(gerAero.buscarCodigo(aero2))) {
												lstPoints.add(
														new MyWaypoint(Color.RED, gerAero.buscarCodigo(aero1).getNome(),
																gerAero.buscarCodigo(aero1).getLocal(), 10));
												lstPoints.add(new MyWaypoint(Color.RED, a.getNome(), a.getLocal(), 10));
												gerenciador.setPontos(lstPoints);
												Tracado tr = new Tracado();
												// Adicionando as mesmas
												// localizações de antes
												tr.addPonto(gerAero.buscarCodigo(aero1).getLocal());
												tr.addPonto(a.getLocal());
												tr.setWidth(4); // largura do
																// traçado
												tr.setCor(Color.RED); // cor do
																		// traçado

												// E adicionando o traçado...
												gerenciador.addTracado(tr);
												gerenciador.getMapKit().repaint();

												lstPoints.add(new MyWaypoint(Color.RED, a.getNome(), a.getLocal(), 10));
												lstPoints.add(new MyWaypoint(Color.RED, b.getNome(), b.getLocal(), 10));
												gerenciador.setPontos(lstPoints);
												Tracado tr2 = new Tracado();
												tr2.addPonto(a.getLocal());
												tr2.addPonto(b.getLocal());
												tr2.setWidth(4); // largura do
																	// traçado
												tr2.setCor(Color.RED); // cor do
																		// traçado

												// E adicionando o traçado...
												gerenciador.addTracado(tr2);
												gerenciador.getMapKit().repaint();

												lstPoints.add(new MyWaypoint(Color.RED, b.getNome(), b.getLocal(), 10));
												lstPoints.add(
														new MyWaypoint(Color.RED, gerAero.buscarCodigo(aero2).getNome(),
																gerAero.buscarCodigo(aero2).getLocal(), 10));
												gerenciador.setPontos(lstPoints);
												Tracado tr3 = new Tracado();
												tr3.addPonto(b.getLocal());
												tr3.addPonto(gerAero.buscarCodigo(aero2).getLocal());
												tr3.setWidth(4); // largura do
																	// traçado
												tr3.setCor(Color.RED); // cor do
																		// traçado

												// E adicionando o traçado...
												gerenciador.addTracado(tr3);
												gerenciador.getMapKit().repaint();
												

											}
										}

									}
								}
							}
						}
					}
				}
			}
		}
		
	}

	public void consulta4(String codAero, int horas) {
		GeoPosition pos = gerenciador.getPosicao();
		List<MyWaypoint> lstPoints = new ArrayList<>();
		Aeroporto a = gerAero.buscarCodigo(codAero);
		for (Rota r : gerRotas.listarTodas()) {
			if (r.getOrigem().equals(a)) {
				VooDireto v = new VooDireto(LocalDateTime.of(2016, 8, 12, 12, 0), r);
				if (v.getDuracao().toHours() <= horas) {
					lstPoints.add(new MyWaypoint(Color.RED, r.getDestino().getNome(), r.getDestino().getLocal(), 10));
					
							
					gerenciador.setPontos(lstPoints);
				}
			}
		}

		for (Rota r1 : gerRotas.listarTodas()) {
			if (r1.getOrigem().equals(a)) {
				for (Rota r2 : gerRotas.listarTodas()) {
					if (r2.getOrigem().equals(r1.getDestino())) {
						VooEscalas v = new VooEscalas(LocalDateTime.of(2016, 8, 12, 12, 0));
						v.adicionarEscala(r1);
						v.adicionarEscala(r2);
						if (v.getDuracao().toHours() <= horas) {
							lstPoints.add(new MyWaypoint(Color.RED, r2.getDestino().getNome(), r2.getDestino().getLocal(), 10));
							gerenciador.setPontos(lstPoints);

							
						}
					}
				}
			}
		}
	}

}
