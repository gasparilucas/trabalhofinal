package modelo;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;

public class VooEscalas extends Voo {
	
	private ArrayList<Rota> rotas;
	
	public VooEscalas(LocalDateTime datahora)
	{
		super(datahora);
		rotas = new ArrayList<>();		
	}
	
	public void adicionarEscala(Rota r) {
		rotas.add(r);
	}
	
	public int getTotalEscalas() { 
		return rotas.size();		
	}
	
	@Override
	public String toString() {
		StringBuilder aux = new StringBuilder();
		for(Rota r: rotas)
			aux.append("\n   "+r);
		return super.toString() + aux.toString();
	}

	@Override
	public Rota getRota() {
		if(rotas.size() > 0)
			return rotas.get(0);
		else
			return null;
	}

	@Override
	public Duration getDuracao() {
		
		int minutos = 0;
		for(Rota r: rotas)
		{
			double dist = r.getOrigem().getLocal()
					.distancia(r.getDestino().getLocal());
			double dur = dist / 805 + 0.5;
			
			minutos = minutos + (int) (dur * 60);
		
		}
		return Duration.ofMinutes(minutos);
	}

}
