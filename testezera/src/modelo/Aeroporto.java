package modelo;

public class Aeroporto implements Comparable<Aeroporto> {
	private String codigo;
	private String nome;
	private Geo loc;
	private int trafego;
	private String idpais; 
	
	public Aeroporto(String codigo, String nome, Geo loc,String idpais) {
		this.codigo = codigo;
		this.nome = nome;
		this.loc = loc;
		trafego = 0;
		this.idpais = idpais;
	}
	
	public String getIdpais() {
		return idpais;
	}
	
	public void setIdpais(String idpais) {
		this.idpais = idpais;
	}
	
	public String getCodigo() {
		return codigo;
	}
	
	public String getNome() {
		return nome;
	}
	
	public Geo getLocal() {
		return loc;
	} 
	
	public int getTrafego(){
		return trafego;
	}
	public void acumTrafego(){
		trafego++;
	}

	@Override
	public int compareTo(Aeroporto o) {
		return nome.compareTo(o.nome);
	}
	
	@Override
	public String toString() {
		return codigo + " - " + nome + " - " + loc + "-" + idpais;
	}
}
