package modelo;

import java.util.ArrayList;

public class GerenciadorPaises {
	private ArrayList<Paises> paises;
	public GerenciadorPaises() {
		paises = new ArrayList<>();
	}
	
	public void adicionar(Paises p) {
		paises.add(p);	
	}
	
	public ArrayList<Paises> listarTodos() {
		return new ArrayList<Paises>(paises);
	}
	
	public Paises buscarCodigo(String codigo) {
		for(Paises p: paises)
			if(p.getCodigo().equals(codigo))
				return p;
		return null;
	}
	public Paises buscarNome(String nome) {
		for(Paises p: paises)
			if(p.getNome().equals(nome))
				return p;
		return null;
	}
	@Override
	public String toString() {
		StringBuilder aux = new StringBuilder();
		for(Paises p: paises)
			aux.append(p + "\n");			
		return aux.toString();
	}

}
